provider "azurerm" {
    #version = "1.34"

    features {}
}

resource "azurerm_resource_group" "rg1" {
  name     = var.ressource_group_name
  location = var.ressource_group_location
}


# Mobile app
resource "azurerm_app_service" "Mobileapp1" {
  name                = var.app_service_name
  location            = azurerm_resource_group.rg1.location
  resource_group_name = azurerm_resource_group.rg1.name
  app_service_plan_id = azurerm_app_service_plan.app_service_plan.id
  https_only          = var.https_only
  site_config {
    php_version = var.app_service_php_version
    always_on   = var.always_on
    ftps_state  = var.ftps_state
  }
}

resource "azurerm_app_service_plan" "app_service_plan" {
  name                 = var.plan_name
  resource_group_name = azurerm_resource_group.rg1.name
  location             = azurerm_resource_group.rg1.location
  kind                 = var.kind
  sku {
    tier     = var.sku_tier
    size     = var.sku_size
    capacity = var.sku_capacity
  }
}


# Traffic Manager

resource "azurerm_traffic_manager_profile" "tmp1" {
  name                = var.tmp_name
  resource_group_name = azurerm_resource_group.rg1.name

  traffic_routing_method = "Weighted"

  dns_config {
    relative_name = var.tmp_name
    ttl           = 100
  }

  monitor_config {
    protocol                     = "http"
    port                         = 80
    path                         = "/"
    interval_in_seconds          = 30
    timeout_in_seconds           = 9
    tolerated_number_of_failures = 3
  }

  tags = {
    environment = "Production"
  }
}

resource "azurerm_traffic_manager_endpoint" "tme1" {
  name                = var.tme_name
  resource_group_name = azurerm_resource_group.rg1.name
  profile_name        = azurerm_traffic_manager_profile.tmp1.name
  target              = "terraform.io"
  type                = "externalEndpoints"
  weight              = 100
}

# API Apps Management Backend
resource "azurerm_api_management" "apim1" {
  name                = var.apim_name
  location            = azurerm_resource_group.rg1.location
  resource_group_name = azurerm_resource_group.rg1.name
  publisher_name      = "clashofshodowgo"
  publisher_email     = "company@terraform.io"

  sku_name = "Developer_1"
}

resource "azurerm_api_management_backend" "apimb1" {
  name                = var.apimb_name
  resource_group_name = azurerm_resource_group.rg1.name
  api_management_name = azurerm_api_management.apim1.name
  protocol            = "http"
  url                 = "https://backend"
}


# Azure CDN
resource "azurerm_cdn_profile" "cdnp1" {
  name                = var.cdnp_name
  location            = azurerm_resource_group.rg1.location
  resource_group_name = azurerm_resource_group.rg1.name
  sku                 = "Standard_Verizon"

  tags = {
    environment = "Production"
    cost_center = "MSFT"
  }
}


# Storage 
resource "azurerm_storage_account" "storage-account" {
  name                     = var.stac_name
  resource_group_name      = azurerm_resource_group.rg1.name
  location                 = azurerm_resource_group.rg1.location
  account_tier             = var.sku_tier
  account_replication_type = "lrs"

  tags = {
    environment = "Production"
  }
}

# Database for MySQL
resource "azurerm_mysql_server" "mysql-server" {
  name                = var.mysqlserv_name
  location            = azurerm_resource_group.rg1.location
  resource_group_name = azurerm_resource_group.rg1.name

  administrator_login          = "admin"
  administrator_login_password = "admin"

  sku_name   = "B_Gen5_2"
  storage_mb = 5120
  version    = "5.7"

  auto_grow_enabled                 = true
  backup_retention_days             = 7
  geo_redundant_backup_enabled      = true
  infrastructure_encryption_enabled = true
  public_network_access_enabled     = false
  ssl_enforcement_enabled           = true
  ssl_minimal_tls_version_enforced  = "TLS1_2"
}

resource "azurerm_mysql_database" "mysqldata1" {
  name                = var.mysqldata_name
  resource_group_name = azurerm_resource_group.rg1.name
  server_name         = azurerm_mysql_server.mysql-server.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

# HDInsight 
/*resource "azurerm_storage_account" "storageanalytics" {
  name                     = var.stacchdin_name
  resource_group_name      = azurerm_resource_group.rg1.name
  location                 = azurerm_resource_group.rg1.location
  account_tier             = var.sku_tier
  account_replication_type = "LRS"
}*/

resource "azurerm_storage_container" "storage-cont" {
  name                  = var.stcont_name
  storage_account_name  = azurerm_storage_account.storage-account.name
  container_access_type = "public"
}

resource "azurerm_hdinsight_hadoop_cluster" "hadoop_cluster" {
  name                = var.hdicluster_name
  resource_group_name = azurerm_resource_group.rg1.name
  location            = azurerm_resource_group.rg1.location
  cluster_version     = "3.6"
  tier                = var.sku_tier

  component_version {
    hadoop = "2.7"
  }

  gateway {
    enabled  = true
    username = "acctestusrgw"
    password = "TerrAform123!"
  }

  storage_account {
    storage_container_id = azurerm_storage_container.storage-cont.id
    storage_account_key  = azurerm_storage_account.storage-account.primary_access_key
    is_default           = true
  }

  roles {
    head_node {
      vm_size  = "Standard_D3_V2"
      username = "acctestusrvm"
      password = "AccTestvdSC4daf986!"
    }

    worker_node {
      vm_size               = "Standard_D4_V2"
      username              = "acctestusrvm"
      password              = "AccTestvdSC4daf986!"
      target_instance_count = 3
    }

    zookeeper_node {
      vm_size  = "Standard_D3_V2"
      username = "acctestusrvm"
      password = "AccTestvdSC4daf986!"
    }
  }
}